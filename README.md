## Instale o módulo via Composer

** Inclua no composer.json do seu projeto Magento 2: **

```
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:rodrigoobiassi/test-backend.git"
    }
]
```

** Comando para instalação: **

```
composer require biassi/module-digitalhub:dev-master
```

** Comando para ativação do módulo: **

```
php bin/magento module:enable Biassi_DigitalHub
```