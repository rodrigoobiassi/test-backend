<?php

namespace Biassi\DigitalHub\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class SendOrderToErp extends AbstractHelper
{
    const XML_PATH_DIGITALHUB = 'erp_integration/general';
    const DIGITALHUB_MODULE_ENABLE = self::XML_PATH_DIGITALHUB . '/enable';
    const DIGITALHUB_LOG_ENABLE = self::XML_PATH_DIGITALHUB . '/enable_log';
    const DIGITALHUB_ERP_HOST = self::XML_PATH_DIGITALHUB . '/host';
    const DIGITALHUB_ERP_API_KEY = self::XML_PATH_DIGITALHUB . '/api_key';

    /**
     * HttpClient constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @param $field
     * @param null $storeId
     * @return mixed
     */
    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @return bool
     */
    public function isModuleEnabled()
    {
        return $this->getConfigValue(self::DIGITALHUB_MODULE_ENABLE);
    }

    /**
     * @return bool
     */
    public function isLogEnabled()
    {
        return $this->getConfigValue(self::DIGITALHUB_LOG_ENABLE);
    }

    /**
     * @return mixed
     */
    public function getErpHost()
    {
        return $this->getConfigValue(self::DIGITALHUB_ERP_HOST);
    }

    /**
     * @return mixed
     */
    public function getErpApiKey()
    {
        return $this->getConfigValue(self::DIGITALHUB_ERP_API_KEY);
    }

    /**
     * @param $order
     * @return mixed
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function integrateOrder($order)
    {
        $body = \GuzzleHttp\json_encode($this->getBodyApi($order));

        $urlApi = $this->getErpHost();
        $apiKey = $this->getErpApiKey();
        $method = 'POST';

        $header = [
            "Content-Type: application/json",
            "Authorization: Bearer " . $apiKey
        ];

        $httpClient = new \Zend\Http\Client();
        $httpClient->setUri($urlApi);
        $httpClient->setHeaders($header);
        $httpClient->setOptions(['timeout' => 30]);
        $httpClient->setMethod($method);
        $httpClient->setRawBody($body);

        try {
            $response = $httpClient->send()->getBody();
            $this->logIntegration(" ([URI: " . $urlApi . "] - [RequestBody: " . $body . "] - [Response: " . \GuzzleHttp\json_encode($response) . " ])");
            return \GuzzleHttp\json_encode($response, true);
        } catch (\Exception $e) {
            $this->logIntegration("ERRO" . $e . "([URI: " . $urlApi . "] - [RequestBody: " . $body . "]");
            $this->_logger->critical($e);
        }
    }

    /**
     * @param $message
     */
    public function logIntegration($message)
    {
        if ($this->isLogEnabled()) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/biassi_digitalhub.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($message);
        }
    }

    /**
     * @param $order
     * @return array
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBodyApi($order)
    {
        $body = [
            "customer" => $this->getCustomer($order),
            "shipping_address" => $this->getShippingAddress($order),
            "items" => $this->getItems($order),
            "shipping_method" => $order->getShippingMethod(),
            "payment_method" => $order->getPayment()->getMethodInstance()->getTitle(),
            "payment_installments" => $this->getPaymentInstallments($order),
            "subtotal" => $order->getSubtotal(),
            "shipping_amount" => $order->getShippingAmount(),
            "discount" => $order->getDiscountAmount(),
            "total" => $order->getGrandTotal()
        ];

        return $body;
    }

    /**
     * Alguns dos dados abaixo como CPF, CNPJ, nome fantasia e etc... podem ser criados de forma customizada ou
     * talvez esteja usando algum módulo existente de terceiro
     *
     * @param $order
     * @return array
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomer($order)
    {
        try {
            $middleName = $order->getCustomerMiddlename() ? ' ' . $order->getCustomerMiddlename() . ' ' : ' ';
            $customer = [
                "name" => $order->getCustomerFirstname() . $middleName . $order->getCustomerLastname(),
                "cpf_cnpj" => $order->getBillingAddress()->getVatId(),
                "telephone" => $order->getBillingAddress()->getTelephone(),
                "cnpj" => $order->getBillingAddress()->getVatId(),
                "razao_social" => $order->getBillingAddress()->getCompany(), // "DIGITAL HUB TECNOLOGIA E DESENVOLVIMENTO",
                "nome_fantasia" => "Digital Hub",
                "ie" => "698.700.800.810",
                "dob" => $order->getCustomerDob()
            ];
        } catch (\Exception $e) {
            $customer = '';
            $this->_logger->critical($e);
        }

        return $customer;
    }

    /**
     * @param $order
     * @return array
     */
    public function getShippingAddress($order)
    {
        try {
            $address = $order->getShippingAddress();
            $viaUrl = 'https://viacep.com.br/ws/' . $address->getPostcode() . '/json/';
            $viaCepResult = json_decode(file_get_contents($viaUrl));

            $shippingAddress = [
                "street" => $address->getStreetLine(1) ? $address->getStreetLine(1) : '',
                "number" => $address->getStreetLine(2) ? $address->getStreetLine(2) : '',
                "additional" => $address->getStreetLine(3) ? $address->getStreetLine(3) : '',
                "neighborhood" => $viaCepResult->bairro,
                "city" => $viaCepResult->localidade,
                "city_ibge_code" => $viaCepResult->ibge,
                "uf" => $viaCepResult->uf,
                "country" => $address->getCountryId()
            ];
        } catch (\Exception $e) {
            $shippingAddress = '';
            $this->_logger->critical($e);
        }

        return $shippingAddress;
    }

    /**
     * @param $order
     * @return array
     */
    public function getItems($order)
    {
        $items = $order->getAllItems();
        $orderItems = [];
        $i = 0;

        foreach ($items as $item) {
            $orderItems[$i]['sku'] = $item->getSku();
            $orderItems[$i]['name'] = $item->getName();
            $orderItems[$i]['price'] = $item->getPrice();
            $orderItems[$i]['qty'] = intval($item->getQtyOrdered());
        }

        return $orderItems;
    }

    /**
     * Valor falso somente para teste, vai depender do módulo de pagamento e estrutura do banco de dados para pegar esse dado!
     *
     * @param $order
     * @return int
     */
    public function getPaymentInstallments($order)
    {
        return 2;
    }
}
