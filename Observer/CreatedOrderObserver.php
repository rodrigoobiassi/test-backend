<?php

namespace Biassi\DigitalHub\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Biassi\DigitalHub\Helper\SendOrderToErp;
use Psr\Log\LoggerInterface;

class CreatedOrderObserver implements ObserverInterface
{
    public $_logger;
    public $_sendOrderToErp;

    /**
     * CreatedOrderObserver constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger,
        SendOrderToErp $sendOrderToErp
    ) {
        $this->_logger = $logger;
        $this->_sendOrderToErp = $sendOrderToErp;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->_sendOrderToErp->isModuleEnabled()) {
            try {
                $order = $observer->getEvent()->getOrder();
                $this->_sendOrderToErp->integrateOrder($order);
            } catch (\Exception $e) {
                $this->_logger->info($e->getMessage());
            }
        }
    }
}
